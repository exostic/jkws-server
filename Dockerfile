FROM node:11 as build

WORKDIR /root/app
COPY . ./
RUN npm install --only=production

FROM node:11-alpine

COPY --from=build /root/app /
EXPOSE 8080
CMD ["npm", "start"]