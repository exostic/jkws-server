/**
 * Module dependencies.
 */

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const jwtConfig = require('./config/jwt')
const rasha = require('rasha')
/**
 * Express configuration.
 */
app.set('host', '0.0.0.0')
app.set('port', process.env.PORT || 8080)
app.set('env', process.env.NODE_ENV || 'development')
app.set('json spaces', 2) // number of spaces for indentation
app.use(cors())
app.use(bodyParser.json())

app.get('/', (req, res) => {res.send(200)}) //For liveness probs

app.get('/jwks', async (req, res, next) => {
  const jwk = {
    ...rasha.importSync({ pem: jwtConfig.publicKey }),
    alg: 'RS256',
    use: 'sig',
    kid: jwtConfig.publicKey
  }
  const jwks = {
    keys: [jwk]
  }
  res.setHeader('Content-Type', 'application/json')
  
  res.status(200).json(jwks)
})

app.listen(app.get('port'), () => {
  console.log(`\nServer launch on http://localhost:${app.get('port')} in mode ${app.get('env')}`);
})

module.exports = app
