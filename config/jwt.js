var fs = require('fs')
var fnv = require('fnv-plus')
var NodeRSA = require('node-rsa');

// TODO: why does rsaPemToJwk work with a file but not with a variable?

let privateKeyPath = "secrets/private-not-use-in-prod.pem"
let privateKey = process.env.AUTH_PRIVATE_KEY || fs.readFileSync(privateKeyPath).toString();;

if(!process.env.AUTH_PRIVATE_KEY) {
  console.log("Missing environment variable AUTH_PRIVATE_KEY");
  console.log("privateKeyPath used: ", privateKeyPath, "\n");
}

exports.key = (privateKey).replace(/\\n/g, '\n')

const key = new NodeRSA(privateKey);
let pubKey = key.exportKey("pkcs8-public");

console.log(pubKey);

exports.publicKey = (pubKey).replace(/\\n/g, '\n')

// Key Identifier – Acts as an ‘alias’ for the key
exports.kid = process.env.AUTH_KEY_ID || fnv.hash(this.publicKey, 128).hex()
